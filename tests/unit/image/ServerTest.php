<?php
namespace mitalcoi\tests\unit\image;
use mitalcoi\rtransport\image\Server;

class ServerTest extends \PHPUnit_Framework_TestCase
{

	public function setUp()
	{
		system("rm -rf " . $this->getPath() . '*');
	}

	public function testEmptyWorkloadUpload()
	{
		$client = new Server();
		$client->setRelativePath($this->getPath());
		$res = $client->imageUpload(\RJSON::encode(array()));
		$this->assertEquals("wrong workload\n", $res);
	}


	public function testUploadWithoutThumbs()
	{
		$res = $this->uploadShmel();
		$this->assertEquals($this->getBaseShmelPath(), $res);
		$res = str_replace('http://bbc.com/', $this->getPath(), $res);
		$this->assertFileExists($res);
		$new = getimagesize($res);
		$old = getimagesize($this->getPath(false) . 'shmel.jpg');
		$this->assertEquals($new, $old);
		$this->assertEquals(1, $this->getFilesCount($this->getPath() . 'iu/shmel/'));
	}

	public function testUploadWithThumbs()
	{
		$res = $this->uploadShmel(array('_160x160.jpg', '_40x40.jpg'));
		$this->assertEquals($this->getBaseShmelPath(), $res);
		$asArray = array();
		foreach (glob($this->getPath() . 'iu/shmel/*.jpg') as $file) {
			$asArray[] = $file;
		}
		$this->assertEquals(3, count($asArray));
		foreach ($asArray as $r) {
			$this->assertFileExists($r);
		}
		$r0 = getimagesize($asArray[0]);
		$r1 = getimagesize($asArray[1]);
		$r2 = getimagesize($asArray[2]);

		$this->assertEquals(1024, $r0[0]);
		$this->assertEquals(160, $r1[0]);
		$this->assertEquals(40, $r2[0]);
		$this->assertEquals(3, $this->getFilesCount($this->getPath() . 'iu/shmel/'));
	}

	public function testSuccessDeleteImage()
	{
		$this->uploadShmel(array('_160x160.jpg', '_40x40.jpg'), true);
		touch($this->getPath() . 'iu/shmel/e/1/7/test1.jpg');
		mkdir($this->getPath() . 'iu/shmel/e/1/8/', 0777, true);
		touch($this->getPath() . 'iu/shmel/e/1/8/test2.jpg');
		$client = new Server();
		$client->setRelativePath($this->getPath());
		$res = $client->imageDelete('http://bbc.com/iu/shmel/e/1/7/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg');
		$asArray = explode("\n", $res);
		$this->assertStringEndsWith(
			'resources/Image/tmp/iu/shmel/e/1/7/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg',
			$asArray[0]
		);
		$this->assertStringEndsWith(
			'resources/Image/tmp/iu/shmel/e/1/7/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg_160x160.jpg',
			$asArray[1]
		);
		$this->assertStringEndsWith(
			'resources/Image/tmp/iu/shmel/e/1/7/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg_40x40.jpg',
			$asArray[2]
		);
		$this->assertEquals(1, $this->getFilesCount($this->getPath() . 'iu/shmel/e/1/7/'));
		$this->assertEquals(1, $this->getFilesCount($this->getPath() . 'iu/shmel/e/1/8/'));
	}

	public function testFailedDeleteImage()
	{
		$this->uploadShmel([], true);
		touch($this->getPath() . 'iu/shmel/e/1/7/test1.jpg');
		mkdir($this->getPath() . 'iu/shmel/e/1/8/', 0777, true);
		touch($this->getPath() . 'iu/shmel/e/1/8/test2.jpg');
		$client = new Server();
		$client->setRelativePath($this->getPath());
		$res = $client->imageDelete('http://bbc.com/iu/');
		$this->assertEquals("strange path: iu/\n", $res);
		$this->assertEquals(2, $this->getFilesCount($this->getPath() . 'iu/shmel/e/1/7/'));
		$this->assertEquals(1, $this->getFilesCount($this->getPath() . 'iu/shmel/e/1/8/'));
	}

	public function testSuccessComplexDeleteImage()
	{
		mkdir($this->getPath() . 'iu/shmel/e/1/8/', 0777, true);
		touch($this->getPath() . 'iu/shmel/e/1/8/test1.jpg');
		touch($this->getPath() . 'iu/shmel/e/1/8/test2.jpg');
		mkdir($this->getPath() . 'iu/shmel/e/1/1/', 0777, true);
		touch($this->getPath() . 'iu/shmel/e/1/1/test1.jpg');
		touch($this->getPath() . 'iu/shmel/e/1/1test2.jpg');
		$client = new Server();
		$client->setRelativePath($this->getPath());
		$client->imageDelete('http://bbc.com/iu/shmel');
		$dir = scandir($this->getPath() . 'iu/');
		$this->assertEquals(array('.', '..'), $dir);
	}


	public function testResize()
	{
		$this->uploadShmel();
		$client = new Server();
		$client->setRelativePath($this->getPath());
		$client->imageResize(
			\RJSON::encode(
				array(
					'url' => $this->getBaseShmelPath(),
					'thumbs' => array('_200x200.jpg')
				)
			)
		);
		$file = $this->getPath(true) . 'iu/shmel/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg_200x200.jpg';
		$this->assertTrue(
			is_file($file)
		);
		$size = getimagesize($file);
		$this->assertEquals(200, $size[0]);
	}

	private function getFilesCount($dir)
	{
		return count(glob($dir . '*.jpg'));
	}

	private function getPath($withTmp = true)
	{
		$p = dirname(__FILE__) . '/../../resources/Image/';
		return ($withTmp ? $p . 'tmp/' : $p);
	}

	private function uploadShmel(array $thumbs = array(), $createSubDirsByHash = false)
	{
		$client = new Server();
		$client->setRelativePath($this->getPath());
		$bytes = utf8_encode(file_get_contents($this->getPath(false) . '/shmel.jpg'));
		return $client->imageUpload(
			\RJSON::encode(
				array(
					'bytes' => $bytes,
					'hash' => md5($bytes),
					'thumbs' => $thumbs,
					'prefix' => 'shmel',
					'upload_domen' => 'http://bbc.com/',
					'quality' => 75,
					'create_subdirs_by_hash' => $createSubDirsByHash
				)
			)
		);
	}

	private function getBaseShmelPath()
	{
		return "http://bbc.com/iu/shmel/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg";
	}
}
