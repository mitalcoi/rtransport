<?php
namespace mitalcoi\tests\unit\mail;
use mitalcoi\rtransport\mail\Client;

class ClientTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        system("rm -rf " . $this->getPath() . '*');
    }

    /**
     * @expectedException \Exception
     */
    public function testException() {
        $client = new Client;
        $client->send();
    }

    /**
     * @expectedException  \Swift_RfcComplianceException
     */
    public function testRfcFailed() {
        $client = new Client;
        $client->setFromMail("from");
        $client->setFromName("from");
        $client->setBody("body");
        $client->setToMail("mail");
        $client->setSubject("subject");
        $client->setRelativePath($this->getPath());
        $res = $client->send();
        $this->assertEquals(array(), $res);
        $this->assertEquals(count(scandir($this->getPath() . 'mails')) - 2, 0);
    }

    public function testSuccessDump() {
        $client = new Client;
        $client->setFromMail("from@mail.com");
        $client->setFromName("from");
        $client->setBody("body");
        $client->setToMail("to@mail.com");
        $client->setSubject("subject");
        $client->setIsDump(true);
        $client->setRelativePath($this->getPath());
        $res = $client->send();
        $this->assertEquals(count(scandir($this->getPath() . 'mails')) - 2, 1);
        $this->assertEquals("body", file_get_contents($this->getPath() . 'mails/to@mail.com.html'));
    }

    private function getPath() {
        $dir = dirname(__FILE__) . '/../../resources/Mail/';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);
        return $dir;
    }

}
