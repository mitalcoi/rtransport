<?php

namespace mitalcoi\rtransport\mail;

class Server extends \mitalcoi\rtransport\BaseServer {

    /**
     * 
     * @param string $data
     * @return string url to file access
     * @throws \Exception
     */
    public function sendMail($data) {
        $data = \RJSON::decode($data);
        if ($data['smtp']) {
            $transport = \Swift_SmtpTransport::newInstance($data['smtp']['host'], $data['smtp']['port'], $data['smtp']['secure'])
                    ->setUsername($data['smtp']['user'])
                    ->setPassword($data['smtp']['pass']);
        } else {
            $transport = \Swift_MailTransport::newInstance();
        }
        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance()
                ->setSubject($data['subject'])
                ->setFrom(array($data['from_mail'] => $data['from_name']))
                ->setTo(array($data['to_mail'] => $data['to_fio']))
                ->setBody($data['body'], 'text/html', 'utf-8');
        if ($data['attachment']) {
            $bytes = utf8_decode($data['attachment']);
            $name = $this->getRelativePath() . uniqid(time()) . '.jpg';
            $file = fopen($name, 'w');
            fwrite($file, $bytes);
            fclose($file);
            $message->attach(\Swift_Attachment::fromPath($name));
        }
        if ($data['isDump']) {
            if (!is_dir($this->getRelativePath() . 'mails/'))
                mkdir($this->getRelativePath() . 'mails/', 0777, true);
            $fp = fopen($this->getRelativePath() . 'mails/' . $data['to_mail'] . '.html', 'a');
            fputs($fp, $data['body']);
            fclose($fp);
            if ($data['attachment'])
                unlink($name);
            return "dumped to " . $this->getRelativePath() . 'mails/' . $data['to_mail'] . '.html';
        } else {
            $mailer->send($message);
            if ($data['attachment'])
                unlink($name);
            sleep($data['sleep']);
            return "sended to " . $data['to_mail'] . "\n";
        }
    }

}
