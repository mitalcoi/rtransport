<?php
namespace mitalcoi\tests\unit\image;
use mitalcoi\rtransport\image\Client;

class ClientTest extends \PHPUnit_Framework_TestCase
{


	public function testUpload()
	{
		$client = new Client();
		$client->setPrefix('shmel');
		$client->setUploadDomen('http://bbc.com/');
		$client->setPath($this->getImage('shmel.jpg'));
		$client->setRelativePath($this->getPath());
		$res = $client->upload();
		$this->assertEquals("http://bbc.com/iu/shmel/e1798c0856bd01aee7ddfcf7b8d5d88f.jpg", $res);
	}

	/**
	 * @expectedException  \Exception
	 */
	public function testException()
	{
		$client = new Client;
		$client->setPath($this->getImage('shmel.jpg'));
		$client->upload();
	}

	private function getImage($name)
	{
		return dirname(__FILE__) . '/../../resources/Image/' . $name;
	}

	private function getPath($withTmp = true)
	{
		$p = dirname(__FILE__) . '/../../resources/Image/';
		return ($withTmp ? $p . 'tmp/' : $p);
	}
}
