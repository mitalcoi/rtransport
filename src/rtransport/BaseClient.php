<?php

namespace mitalcoi\rtransport;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class BaseClient extends Base
{
	protected $_rServer;

	protected function doit($name, $params, $server)
	{
		if (is_array($params)) {
			$params = \RJSON::encode($params);
		}
		if ($this->getRServer()) {
			$server = $this->getRServer();
			$connection = new AMQPConnection($server['server'], $server['port'], $server['user'], $server['password']);
			$channel = $connection->channel();
			$channel->queue_declare($name, false, false, false, false);
			$msg = new AMQPMessage($params);
			$channel->basic_publish($msg, '', $name);
			$channel->close();
			$connection->close();
		} else {
			$server->setRelativePath($this->getRelativePath());
			$server->$name($params);
		}
	}

	public function getRServer()
	{
		return $this->_rServer;
	}

	public function setRServer($m)
	{
		if ($m) {
			if (count($m) !== 4) {
				throw new \Exception("Wrong argument count");
			}
			if (!isset($m['server']) || !isset($m['port']) || !isset($m['user']) || !isset($m['password'])) {
				throw new \Exception("Wrong config");
			}
			$this->_rServer = $m;
		}
	}

}
