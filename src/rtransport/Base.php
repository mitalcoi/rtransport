<?php

namespace mitalcoi\rtransport;

class Base {

    protected $_rServer, $_relativePath;

    protected function normalize($url) {
        if (substr($url, -1) !== '/')
            $url .= '/';
        return $url;
    }

    public function getRelativePath() {
        $rp = $this->_relativePath;
        if (!$rp)
            throw new \Exception('relative path must be set!!');
        return $rp;
    }

    public function setRelativePath($path) {
        $path = $this->normalize($path);
        $this->_relativePath = $path;
    }

}
