<?php

namespace mitalcoi\rtransport\mail;

class Client extends \mitalcoi\rtransport\BaseClient implements \IRTransportClient {

    private $_toMail, $_toFio, $_subject, $_body, $_fromMail, $_fromName, $_attachment, $_smtp, $_sleep, $_isDump;

    public function send() {
        $res = array(
            'to_mail' => $this->getToMail(),
            'to_fio' => $this->getToFio(),
            'subject' => $this->getSubject(),
            'body' => $this->getBody(),
            'from_name' => $this->getFromName(),
            'from_mail' => $this->getFromMail(),
            'attachment' => $this->getAttachment(),
            'smtp' => $this->getSmtp(),
            'sleep' => $this->getSleep(),
            'isDump' => $this->getIsDump()
        );
        $this->doWork("sendMail", $res);
        return $res;
    }

    public function setToMail($mail) {
        $this->_toMail = $mail;
    }

    public function doWork($name, $params) {
        $this->doit($name, $params, new Server());
    }

    public function getToMail() {
        if (!$this->_toMail)
            throw new \Exception('toMail must be setted');
        return $this->_toMail;
    }

    public function setToFio($fio) {
        $this->_toFio = $fio;
    }

    public function getToFio() {
        return $this->_toFio;
    }

    public function setSubject($s) {
        $this->_subject = $s;
    }

    public function getSubject() {
        return $this->_subject;
    }

    public function setBody($b) {
        $this->_body = $b;
    }

    public function getBody() {
        if (!$this->_body)
            throw new \Exception('body must be setted');
        return $this->_body;
    }

    public function setFromMail($f) {
        $this->_fromMail = $f;
    }

    public function getFromMail() {
        if (!$this->_fromMail)
            throw new \Exception('fromMail must be setted');
        return $this->_fromMail;
    }

    public function getFromName() {
        if (!$this->_fromName)
            throw new \Exception('fromName must be setted');
        return $this->_fromName;
    }

    public function setFromName($f) {
        $this->_fromName = $f;
    }

    public function setAttachment($a) {
        $this->_attachment = $a;
    }

    public function getAttachment() {
        return $this->_attachment;
    }

    public function setSmtp(array $s) {
        foreach (array('user', 'pass', 'host', 'port', 'secure') as $el) {
            if (!isset($s[$el]) || $s[$el] === null)
                throw new \Exception('smtp array must contain non-empty' . $el);
        }
        $this->_smtp = $s;
    }

    public function getSmtp() {
        return $this->_smtp;
    }

    public function setIsDump($d) {
        $this->_isDump = $d;
    }

    public function getIsDump() {
        return $this->_isDump;
    }

    public function getSleep() {
        $s = $this->_sleep;
        if (!$s)
            $s = 2;
        return $s;
    }

    public function setSleep($s) {
        $this->_sleep = $s;
    }

}

