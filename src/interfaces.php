<?php

interface IRTransportClient {

    public function doWork($name, $params);
}