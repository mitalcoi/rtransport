<?php

namespace mitalcoi\rtransport\image;

class Client extends \mitalcoi\rtransport\BaseClient implements \IRTransportClient
{

	private $_thumbs = array();
	private $_uploadDomen;
	private $_prefix;
	private $_path;
	private $_createSubdirsByHash = false;
	private $_quality = 75;

	public function delete()
	{
		$this->doWork("imageDelete", $this->getPath());
	}

	public function upload()
	{
		$bytes = file_get_contents($this->getPath());
		$bytes = utf8_encode($bytes);
		$hash = md5($bytes);
		$filename = $hash . '.jpg';
		$meta = '';
		if ($this->getCreateSubdirsByHash()) {
			$meta = '/' . $hash[0] . '/' . $hash[1] . '/' . $hash[2];
		}
		$saveName = $this->getUploadDomen() . 'iu/' . $this->getPrefix() . $meta . '/' . $filename;
		$this->doWork("imageUpload", array('bytes' => $bytes, 'hash' => $hash, 'thumbs' => $this->getThumbs(), 'prefix' => $this->getPrefix(), 'upload_domen' => $this->getUploadDomen(), 'quality' => $this->getQuality(), 'create_subdirs_by_hash' => $this->getCreateSubdirsByHash()));
		return $saveName;
	}

	public function resize()
	{
		$this->doWork(
			"imageResize",
			array(
				'url' => $this->getPath(),
				'thumbs' => $this->getThumbs()
			)
		);
	}

	public function doWork($name, $params)
	{
		$this->doit($name, $params, new Server());
	}

	public function setThumbs(array $thumbs = array())
	{
		$this->_thumbs = $thumbs;
	}

	public function getThumbs()
	{
		return $this->_thumbs;
	}

	public function setUploadDomen($domen)
	{
		$this->_uploadDomen = $domen;
	}

	public function getUploadDomen()
	{
		if (!$this->_uploadDomen) {
			throw new \Exception('upload domen not setted!');
		}
		return $this->_uploadDomen;
	}

	public function setPrefix($px)
	{
		$this->_prefix = $px;
	}

	public function getPrefix()
	{
		if (!$this->_prefix) {
			throw new \Exception('prefix not setted!');
		}
		return $this->_prefix;
	}

	public function setPath($px)
	{
		$this->_path = $px;
	}

	public function getPath()
	{
		if (!$this->_path) {
			throw new \Exception('path not setted!');
		}
		return $this->_path;
	}

	public function setQuality($q)
	{
		$this->_quality = $q;
	}

	public function getQuality()
	{
		return $this->_quality;
	}

	/**
	 * @return mixed
	 */
	public function getCreateSubdirsByHash()
	{
		return (boolean)$this->_createSubdirsByHash;
	}

	/**
	 * @param mixed $createSubdirsByHash
	 */
	public function setCreateSubdirsByHash($createSubdirsByHash)
	{
		$this->_createSubdirsByHash = $createSubdirsByHash;
	}

}

