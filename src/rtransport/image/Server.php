<?php

namespace mitalcoi\rtransport\image;

class Server extends \mitalcoi\rtransport\BaseServer
{

	/**
	 *
	 * @param string $data
	 * @return string url to file access
	 * @throws \Exception
	 */
	public function imageUpload($data)
	{
		if (!$this->getRelativePath()) {
			throw new \Exception('relative path not set!');
		}
		@$data = \RJSON::decode($data);
		if (!isset($data['create_subdirs_by_hash']) || !isset($data['bytes']) || !isset($data['hash']) || !isset($data['thumbs']) || !isset($data['prefix']) || !isset($data['upload_domen']) || !isset($data['quality'])) {
			return "wrong workload\n";
		} else {
			$bytes = utf8_decode($data['bytes']);
			$hash = $data['hash'];
			$filename = $hash . '.jpg';
			$meta = '';
			if ($data['create_subdirs_by_hash']) {
				$meta = $hash[0] . '/' . $hash[1] . '/' . $hash[2] . '/';
			}
			$path = $this->getRelativePath() . 'iu/' . $data['prefix'] . '/' . $meta;
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}
			$im = imagecreatefromstring($bytes);
			imagejpeg($im, $path . $filename, $data['quality']);
			imageDestroy($im);
			if ($data['thumbs']) {
				$this->createThumbnail($path . $filename, $data['thumbs']);
			}
			return str_replace($this->getRelativePath(), $data['upload_domen'], $path . $filename);
		}
	}

	private function createThumbnail($path, array $thumbs)
	{
		$imagine = new \Imagine\Gd\Imagine();
		$image = $imagine->open($path);
		foreach ($thumbs as $thumb) {
			if ($thumb) {
				preg_match('/_([0-9]+)x/', $thumb, $mathes);
				if (isset($mathes[1])) {
					$size = $mathes[1];
					$image->thumbnail(new \Imagine\Image\Box($size, $size))->save($path . $thumb);
				}
			}
		}
	}

	public function imageDelete($path)
	{
		if (!$this->getRelativePath()) {
			throw new \Exception('relative path not set!');
		}
		if (!$path) {
			return "wrong workload\n";
		} else {
			$path = preg_replace('#http://[^/]*/#', '', $path);
			if (preg_match('#iu/.+#', $path)) {
				$path = $this->getRelativePath() . $path;
				if (preg_match('#iu/.+#', $path) || preg_match('#old/.+#', $path)) {
					if (is_dir($path)) {
						return $this->rrmdir($path);
					} else {
						$return = '';
						foreach (glob($path . '*') as $del) {
							if (unlink($del)) {
								$return .= "deleted: $del\n";
							}
						}
						return $return;
					}
				}
			} else {
				return "strange path: $path\n";
			}
		}
	}

	/**
	 * @param string $data
	 * @return string
	 * @throws \Exception
	 */
	public function imageResize($data)
	{
		@$data = \RJSON::decode($data);
		if (!$this->getRelativePath()) {
			throw new \Exception('relative path not set!');
		}
		if (!isset($data['url']) || !isset($data['thumbs'])) {
			return "wrong workload\n";
		}
		$split = preg_split('/iu\//', $data['url']);
		if (isset($split[1])) {
			$pic = $this->getRelativePath() . '/iu/' . $split[1];
			if (is_file($pic)) {
				$this->createThumbnail($pic, $data['thumbs']);
			}

		}

	}

	private function rrmdir($dir)
	{
		static $return = '';
		foreach (glob($dir . '/*') as $file) {
			if (is_dir($file)) {
				$this->rrmdir($file);
			} else {
				unlink($file);
				$return .= "deleted: $file\n";
			}
		}
		rmdir($dir);
		return $return;
	}

}
